<?php
//  config
$sendHeader404	= TRUE;

//  identify environment
$path	= getEnv( 'REQUEST_URI' );
$uri	= getEnv( 'DOCUMENT_ROOT' ).$path;
$host	= getEnv( 'HTTP_HOST' );

if( !file_exists( $uri ) ){
	header( 'HTTP/1.0 404 Not Found' );
	die( "<h1>404 Not Found</h1>Invalid path: ".$path );
}
if( $sendHeader404 )
	header( 'HTTP/1.0 404 Not Found' );

//  index folders
$folders	= array();
foreach( new DirectoryIterator( $uri ) as $entry )
	if( $entry->isDir() && strpos( $entry->getFilename(), "." ) !== 0 )
		$folders[$entry->getPathname()] = $entry->getFilename();
natcasesort( $folders );

//  index files
$files	= array();
foreach( new DirectoryIterator( $uri ) as $entry )
	if( !$entry->isDir() && strpos( $entry->getFilename(), "." ) !== 0 )
		$files[$entry->getPathname()] = $entry->getFilename();
natcasesort( $files );

//  display rendered output
$pathSelf	= dirname( __FILE__ ).'/';
$style		= file_get_contents( $pathSelf.'style.css' );
$script		= file_get_contents( $pathSelf.'script.js' );
$html		= require_once( $pathSelf.'view.phpt' );
print( $html );
