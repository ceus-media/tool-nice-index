<?php

/*  --  POSITION  --  */
$parts	= array();
if( $path !== '/' )
	$parts	= explode( "/", trim( preg_replace( "/\/(.*)\//", "\\1", $path ) ) );

$way	= "/";
$steps	= array( '/' => '<b>'.getEnv( 'HTTP_HOST' ).'</b>' );
foreach( $parts as $part ){
	$steps[$way.$part]	= $part;
	$way	.= $part === "/" ? "/" : $part.'/';
}
$last	= @array_pop( array_keys( $steps ) );
foreach( $steps as $url => $label ){
	$link			= '<a href="'.( $url === "/" ? "/" : $url.'/' ).'">'.$label.'</a>';
	$steps[$url]	= '<li class="'.( $last === $url ? 'active' : '' ).'">'.$link.'</li>';
}
array_shift( $steps );
$steps	= join( array_values( $steps ) );

/*  --  FOLDER LIST  --  */
$list	= array();
foreach( $folders as $pathname => $filename ){
	$url	= './'.rawurlencode( $filename ).'/';
	$class	= 'btn btn-large btn-info';
	$list[]	= '<li><a href="'.$url.'" class="'.$class.'">'.$filename.'</a></li>';
}
$folders	= $list ? '<ul class="unstyled folders">'.join( $list ).'</ul>' : '';

/*  --  FILE LIST  --  */
$list	= array();
foreach( $files as $pathname => $filename ){
	$url	= './'.rawurlencode( $filename );
	$class	= 'btn btn-primary';
	$list[]	= '<li><a href="'.$url.'" class="'.$class.'">'.$filename.'</a></li>';
}
$files	= $list ? '<ul class="unstyled files">'.join( $list ).'</ul>' : '';

return '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta http-equiv="Content-Script-Type" content="text/javascript"/>
		<meta http-equiv="Content-Style-Type" content="text/css"/>
		<meta name="viewport" content="width=device-width, user-scalable=no"/>
		<title>'.$path.' @ '.$host.'</title>
		<link rel="stylesheet" type="text/css" media="all" href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css"/>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/headjs/1.0.3/head.core.min.js"></script>
		<script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
		<style type="text/css">'.$style.'</style>
		<script type="text/javascript">'.$script.'</script>
	</head>
	<body>
		<div class="navbar navbar-not-inverse navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<!-- .btn-navbar is used as the toggle for collapsed navbar content -->
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<a class="brand" href="/">'.$host.'</a>
					<div class="nav-collapse collapse">
						<ul class="nav">
							'.$steps.'
						</ul>
						<ul class="nav pull-right">
							<div class="navbar-search">
								<input type="text" class="search-query" placeholder="Search">
							</div>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="container" id="buttons">
			<div class="row">
				'.$folders.'
			</div>
			<div class="row">
				'.$files.'
			</div>
			<br/>
		</div>
	<body>
</html>';