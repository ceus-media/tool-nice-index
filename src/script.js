(function($){
	$(document).ready(function(){
		$("input.search-query").on("keyup", function(event){
			applySearch($(this).val());
//			if($(this).hasClass('changed'))
			if(event.keyCode === 13){
				$("#buttons li:visible:eq(0) a").each(function(){
					document.location.href = $(this).attr("href");
				});
			}
			$(this).addClass('changed');
		}).focus();
		applySearch($("input.search-query").val());

/*		$(window).on("resize orientationchange init", function(event){
			$("#data").html(event.type);//$("html").attr("class"));
		}).trigger("init");*/
	});
})(jQuery);

function applySearch(term){
	$("#buttons ul li").each(function(){
		if(!term.length)
			return $(this).show();
		var link = $(this).children("a");
//		console.log(term.toUpperCase());
		console.log(link.html().toUpperCase()+" -> "+( link.html().toUpperCase().indexOf(term.toUpperCase()) >= 0 ));
		var contains = link.html().toUpperCase().indexOf(term.toUpperCase()) >= 0;
		contains ? $(this).show() : $(this).hide();
	});
}
