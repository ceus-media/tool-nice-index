#!/bin/bash

# --  CONFIGURE ME!
PATH_INDEX="/var/www/"
PATH_TOOL="/var/www/.index/"
FS_USER="kriss"
FS_GROUP="www-data"


# --  NO CHANGES NEEDED BELOW
git clone git@gitlab.com:ceus-media/tool-nice-index.git
test -e $PATH_TOOL && rm -R $PATH_TOOL
mv tool-nice-index/src $PATH_TOOL
rm -Rf tool-nice-index
sed -i "s@path/to/NiceIndex/@$PATH_TOOL@" $PATH_TOOL.htaccess.install
mv $PATH_TOOL.htaccess.install $PATH_INDEX.htaccess
sudo chown -R $FS_USER:$FS_GROUP $PATH_TOOL
