## Nice Index

### Installation

#### Manually

- Download & extract or checkout to a work folder.
- Think of a folder where the tool should be located, lets say <code>.index</code> in folder <code>/var/www</code>.
- Copy the tools folder <code>src</code> to the folder you thought, so for exampe <code>/var/www/.index</code>.
- Set rights for your webserver to target folder.
- Apply catch-all by applying .htaccess.install to the .htaccess where the index should begin to work.

Here is an example:
````
git clone git@gitlab.com:ceus-media/tool-nice-index.git
mv tool-nice-index/src /var/www/.index
rm -Rf tool-nice-index
cd /var/www/.index
sed -i 's@path/to/NiceIndex@/var/www/.index@' .htaccess.install
mv .htaccess.install /var/www/.htaccess
sudo chown -r kriss:www-data .
````

#### Script

Configure file <code>install.sh</code> and run it.

It will:

- place the tool files into the configured tool folder
- install .htaccess file in folder to apply index to
- set rights

Attention, it will also:
- remove the target tool folder if existing
- replace an existing .htaccess file within the target index path

Run it like this:
````
chmod +x install.sh
./install.sh
````

